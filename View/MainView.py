from tkinter import *
from tkinter import filedialog

from Model.StatisticalSummarizer import StatisticalSummarizer


class Window(Frame):

    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.master = master
        self.init_window()

    file_name = ''

    def file_upload(self):
        Window.file_name = filedialog.askopenfilename()
        # upload_label.config(text=filename)
        print(Window.file_name)
        return Window.file_name

    def generate_summary(self):
        # self.second_window()
        print("this is a test")
        x = "hi hi hi hi"

        # summary = StatisticalSummarizer.summarizer(Window.file_name)
        # for sent in summary:
        #     print(sent)
        # # return summary
        return x

    def load_main_window(self):
        # self.destroy()
        root.update()
        root.deiconify()

    def second_window(self):
        root.withdraw()
        top = Toplevel()
        top.title("Generated Reports")
        top.grid_rowconfigure(0, weight=1)
        top.grid_columnconfigure(0, weight=1)
        w, h = top.winfo_screenwidth(), top.winfo_screenheight()
        top.geometry("%dx%d+0+0" % (w, h))
        # top.pack(fill=BOTH, expand=YES)

        # label defining the topic of the view
        topic_label = Label(top, text="MyTutor", width=55, fg="white", bg="black",
                            font="Verdana 26 bold")
        topic_label.place(x=0, y=0)

        # summary topic label
        summary_label = Label(top, text="Summarized Short Note", fg="black", font="Verdana 14 bold")
        summary_label.place(x=200, y=70)

        # scrollable text input box
        text_pad_summary = Frame(top)
        text_pad_summary.place(x=20, y=120)
        text_area_summary = Text(text_pad_summary, height=30, width=80)
        text_area_summary.insert(INSERT, self.generate_summary)
        scroll_bar_summary = Scrollbar(text_pad_summary)
        text_area_summary.configure(yscrollcommand=scroll_bar_summary.set)
        text_area_summary.pack(side=LEFT)
        scroll_bar_summary.pack(side=RIGHT, fill=Y)

        # question list topic label
        qlist_label = Label(top, text="Factual Question List", fg="black", font="Verdana 14 bold")
        qlist_label.place(x=900, y=70)

        # scrollable text input box
        text_pad_q = Frame(top)
        text_pad_q.place(x=690, y=120)
        text_area_q = Text(text_pad_q, height=30, width=80)
        scroll_bar_q = Scrollbar(text_pad_q)
        text_area_q.configure(yscrollcommand=scroll_bar_q.set)
        text_area_q.pack(side=LEFT)
        scroll_bar_q.pack(side=RIGHT, fill=Y)

        # refresh button
        refresh_button = Button(top, text="Refresh", fg="white", bg="black", font="Verdana 12 bold")
        refresh_button.place(x=1150, y=640)

        # back button
        back_button = Button(top, text="Back", fg="white", bg="black", font="Verdana 12 bold", width=7,
                             command=self.load_main_window)
        back_button.place(x=1250, y=640)

    def init_window(self):
        self.master.title("MyTutor")
        self.pack(fill=BOTH, expand=YES)

        # label defining the topic of the view
        topic_label = Label(self, text="MyTutor", width=55, fg="white", bg="black",
                            font="Verdana 26 bold")
        topic_label.place(x=0, y=0)

        # scrollable text input box
        text_pad = Frame()
        text_pad.place(x=275, y=90)
        text_area = Text(text_pad, height=25, width=100)
        text_area.insert(INSERT, "\n Paste your text here..")
        text_area.insert(END, "\n\n\n You can also upload your file.")
        text_area.tag_add("Paste", "2.0", "2.22")
        text_area.tag_add("You", "5.0", "5.31")
        text_area.tag_config("Paste", foreground="black", font="Verdana 12 bold")
        text_area.tag_config("You", foreground="black", font="Verdana 11")
        scroll_bar = Scrollbar(text_pad)
        text_area.configure(yscrollcommand=scroll_bar.set)
        text_area.pack(side=LEFT)
        scroll_bar.pack(side=RIGHT, fill=Y)

        # button for text file uploading
        upload_button = Button(self, text="Upload", fg="white", bg="black", font="Verdana 11 bold",
                               command=self.file_upload)
        upload_button.place(x=550, y=512)

        # label defining the upload button
        upload_label = Label(self, text="Upload your file from here", fg="black", font="Verdana 11")
        upload_label.place(x=630, y=514)

        # button for text summarization function
        summarize_button = Button(self, text="Summarize", fg="white", bg="black", font="Verdana 18 bold", width=13,
                                  command=self.second_window)
        summarize_button.place(x=360, y=580)

        # button for question generation function
        q_generation_button = Button(self, text="Question Paper", fg="white", bg="black", font="Verdana 18 bold")
        q_generation_button.place(x=760, y=580)


root = Tk()
root.grid_rowconfigure(0, weight=1)
root.grid_columnconfigure(0, weight=1)
w, h = root.winfo_screenwidth(), root.winfo_screenheight()
root.geometry("%dx%d+0+0" % (w, h))
# root.geometry("400x300")
app = Window(root)
root.mainloop()
