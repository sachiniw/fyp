from tkinter import *
import tkinter as tk
from tkinter import filedialog
from Model.StatisticalSummarizer import StatisticalSummarizer
from Model.QuestionTransducer import Transducer

LARGE_FONT = ("Verdana", 12)


class MyTutor(tk.Tk):
    summary = ''

    def __init__(self, *args, **kwargs):

        tk.Tk.__init__(self, *args, **kwargs)
        container = tk.Frame(self)

        container.pack(side="top", fill="both", expand=True)

        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)
        w, h = container.winfo_screenwidth(), container.winfo_screenheight()
        self.geometry("%dx%d+0+0" % (w, h))

        self.frames = {}

        for F in (StartPage, ReportPage):
            frame = F(container, self)
            self.frames[F] = frame
            frame.grid(row=0, column=0,sticky="nsew")

        self.show_frame(StartPage)

    def show_frame(self, cont):
        frame = self.frames[cont]
        frame.tkraise()

    def upload_file(self,file_name,label_x):
        file_name = filedialog.askopenfilename()
        label_x.config(text=file_name)

    def view_summary(self,cont,file,text_area):
        text_area.delete('1.0', END)
        summary = StatisticalSummarizer.summarizer(file)
        for sent in summary:
            text_area.insert(INSERT,'• ')
            text_area.insert(INSERT,sent)
            text_area.insert(INSERT,'\n')

    def view_q_list(self,cont,file,text_area):
        text_area.delete('1.0', END)
        q_list = Transducer.np_based_questions(file)
        for Q in q_list:
            text_area.insert(INSERT,'• ')
            text_area.insert(INSERT,Q)
            text_area.insert(INSERT,'\n')




class StartPage(tk.Frame):


    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        self.pack(fill=BOTH, expand=YES)

        # label defining the topic of the view
        topic_label = Label(self, text="MyTutor", width=55, fg="white", bg="black",
                            font="Verdana 26 bold")
        topic_label.place(x=0, y=0)

        # scrollable text input box for summary
        text_pad = Frame(self)
        text_pad.place(x=90, y=175)
        text_area = Text(text_pad, height=30, width=70)
        # text_area.insert(INSERT, "\n Paste your text here..")
        # text_area.insert(END, "\n\n\n You can also upload your file.")
        text_area.tag_add("Paste", "2.0", "2.22")
        text_area.tag_add("You", "5.0", "5.31")
        text_area.tag_config("Paste", foreground="black", font="Verdana 12 bold")
        text_area.tag_config("You", foreground="black", font="Verdana 11")
        scroll_bar = Scrollbar(text_pad)
        text_area.configure(yscrollcommand=scroll_bar.set)
        text_area.pack(side=LEFT)
        scroll_bar.pack(side=RIGHT, fill=Y)

        # scrollable text input box for question list
        text_pad_q = Frame(self)
        text_pad_q.place(x=700, y=175)
        text_area_q = Text(text_pad_q, height=30, width=70)
        # text_area.insert(INSERT, "\n Paste your text here..")
        # text_area.insert(END, "\n\n\n You can also upload your file.")
        text_area_q.tag_add("Paste", "2.0", "2.22")
        text_area_q.tag_add("You", "5.0", "5.31")
        text_area_q.tag_config("Paste", foreground="black", font="Verdana 12 bold")
        text_area_q.tag_config("You", foreground="black", font="Verdana 11")
        scroll_bar_q = Scrollbar(text_pad_q)
        text_area_q.configure(yscrollcommand=scroll_bar_q.set)
        text_area_q.pack(side=LEFT)
        scroll_bar_q.pack(side=RIGHT, fill=Y)

        # label defining the upload button
        upload_label = Label(self, text="Upload your file from here", fg="black", font="Verdana 11")
        upload_label.place(x=630, y=70)

        # button for text file uploading
        upload_button = Button(self, text="Upload", fg="white", bg="black", font="Verdana 11 bold",
                               command=lambda: controller.upload_file("",upload_label))
        upload_button.place(x=550, y=70)

        # upload_label.cget("text")
        # button for text summarization function
        summarize_button = Button(self, text="Summarize", fg="white", bg="black", font="Verdana 14 bold", width=12,
                                  command=lambda: controller.view_summary(StartPage,upload_label.cget("text"),text_area))
        summarize_button.place(x=250, y=120)

        # button for question generation function
        q_generation_button = Button(self, text="Question Paper", fg="white", bg="black", font="Verdana 14 bold",
                                     command=lambda: controller.view_q_list(StartPage, upload_label.cget("text"),text_area_q))
        q_generation_button.place(x=900, y=120)



class ReportPage(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        # label defining the topic of the view
        topic_label = Label(self, text="MyTutor", width=55, fg="white", bg="black",
                            font="Verdana 26 bold")
        topic_label.place(x=0, y=0)

        # summary topic label
        summary_label = Label(self, text="Summarized Short Note", fg="black", font="Verdana 14 bold")
        summary_label.place(x=200, y=70)

        # scrollable text input box
        text_pad_summary = Frame(self)
        text_pad_summary.place(x=20, y=120)
        text_area_summary = Text(text_pad_summary, height=30, width=80)
        text_area_summary.insert(INSERT, MyTutor.summary)
        scroll_bar_summary = Scrollbar(text_pad_summary)
        text_area_summary.configure(yscrollcommand=scroll_bar_summary.set)
        text_area_summary.pack(side=LEFT)
        scroll_bar_summary.pack(side=RIGHT, fill=Y)

        # question list topic label
        qlist_label = Label(self, text="Factual Question List", fg="black", font="Verdana 14 bold")
        qlist_label.place(x=900, y=70)

        # scrollable text input box
        text_pad_q = Frame(self)
        text_pad_q.place(x=690, y=120)
        text_area_q = Text(text_pad_q, height=30, width=80)
        scroll_bar_q = Scrollbar(text_pad_q)
        text_area_q.configure(yscrollcommand=scroll_bar_q.set)
        text_area_q.pack(side=LEFT)
        scroll_bar_q.pack(side=RIGHT, fill=Y)

        # refresh button
        refresh_button = Button(self, text="Refresh", fg="white", bg="black", font="Verdana 12 bold")
        refresh_button.place(x=1150, y=640)

        # back button
        back_button = Button(self, text="Back", fg="white", bg="black", font="Verdana 12 bold", width=7,
                             command=lambda: controller.show_frame(StartPage))
        back_button.place(x=1250, y=640)


if __name__ == "__main__":
    app = MyTutor()
    app.mainloop()