from os import path
import nltk
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer

from Model.SentenceSimplificationModule import SentenceSimplifier
from Model.TextPreprocessingModule import TextPreprocessor


class Feature02:

    def tokenize(text):
        tokens = nltk.word_tokenize(text)
        stems = []
        for item in tokens:
            stems.append(PorterStemmer().stem(item))
        return stems

    def get_topic_feature_rate(paragraphs, topic_list):
        # list to store sentence and rating tuple
        sent_topic_rate_list = []

        topic_feature_rate = 0
        topic_words = []
        for t in topic_list:
            tokenized_topics = Feature02.tokenize(' '.join(t))
            for tp in tokenized_topics:
                topic_words.append(tp)

        # remove duplicates in the list of words inn topic
        topic_words = list(set(topic_words))

        # removing stop words from the topic_words list
        stop_words = set(stopwords.words('english'))
        refined_topic_words = list(set(topic_words) - set(stop_words))
        words_in_topic_count = len(refined_topic_words)

        for para in paragraphs:
            for sent in para:
                tokenized_sent = Feature02.tokenize(sent)
                sent_tw_count = 0
                for rft in refined_topic_words:
                    sent_tw_count += tokenized_sent.count(rft)
                # calculate topic feature rating
                if words_in_topic_count != 0:
                    topic_feature_rate = round((sent_tw_count / words_in_topic_count), 4)
                else:
                    topic_feature_rate = 0

                sent_topic_rate_list.append((sent, topic_feature_rate))

        return sent_topic_rate_list


if __name__ == "__main__":
    file = path.expanduser('E:/Level 6/FYP/Implementation/DataSet/Mount Everest/Physical Features.txt')
    simplified_sent_list = SentenceSimplifier.simplifier(file)
    topic_list = TextPreprocessor.get_topics(file)
    print(Feature02.get_topic_feature_rate(simplified_sent_list, topic_list))
