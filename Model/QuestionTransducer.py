from os import path
import string
from nltk.util import breadth_first

from Model.TextPreprocessingModule import TextPreprocessor
from Model.SentenceSimplificationModule import SentenceSimplifier

class Transducer:

    def findNP(sentence):
        parsed_sent = TextPreprocessor.sent_parse(sentence)
        for node in breadth_first(parsed_sent, -1):
            for nodule in node:

                if nodule.label() == 'NP':
                    NP = nodule.leaves()
                    break
                else:
                    NP = None
                    # print(nodule.leaves())

        return NP

    def findVP(sentence):
        parsed_sent = TextPreprocessor.sent_parse(sentence)
        for node in breadth_first(parsed_sent, -1):
            for nodule in node:
                VP = None
                if nodule.label() == 'VP':
                    VP = nodule.leaves()
                    # print(nodule.leaves())
                    break
        return VP

    def findPP(sentence):
        parsed_sent = TextPreprocessor.sent_parse(sentence)
        for node in breadth_first(parsed_sent, -1):
            for nodule in node:

                if nodule.label() == 'PP':
                    PP = nodule.leaves()
                    break
                else:
                    PP = None

        return PP

    def findSBAR(sentence):
        parsed_sent = TextPreprocessor.sent_parse(sentence)
        for node in breadth_first(parsed_sent, -1):
            for nodule in node:
                if node.label() == 'SBAR':
                    print("found")
                    print(node.leaves())

    def find_answer_phrase(sentence):
        answer_phrases = []
        tagged_sent = TextPreprocessor.ner_tagger(sentence)
        for tagged_word in tagged_sent:
            if tagged_word[1] in ['LOCATION', 'PERSON', 'ORGANIZATION']:
                answer_phrases.append((tagged_word[0], tagged_word[1]))

        return answer_phrases

    def np_based_questions(file_):
        np_question_list = []
        simplified_sent_list = SentenceSimplifier.simplifier(file_)
        for para in simplified_sent_list:
            for sent in para:

                noun_phrase = Transducer.findNP(sent)
                tagged_phrase = Transducer.find_answer_phrase(' '.join(noun_phrase))
                # print("NP: ")
                # print(noun_phrase)
                # print("tagged: ")
                # print(tagged_phrase)
                answer_phrase = []
                for p in tagged_phrase:
                    answer_phrase.append(p[0])
                # if set(noun_phrase) < set(answer_phrase):
                #     print("success")
                if any(word in noun_phrase for word in answer_phrase):
                    # print("success")
                    if tagged_phrase[0][1] == "LOCATION":
                        question = sent.replace(' '.join(noun_phrase), "Where")
                        np_question_list.append(question)
                        # print(question)
                        # print("============================location==================================")
                    elif tagged_phrase[0][1] == "PERSON":
                        question = sent.replace(' '.join(noun_phrase), "Who")
                        np_question_list.append(question)
                        # print(question)
                        # print("==========================person====================================")
                    elif tagged_phrase[0][1] == "ORGANIZATION":
                        question = sent.replace(' '.join(noun_phrase), "What")
                        np_question_list.append(question)
                        # print(question)
                        # print("==========================organization====================================")

        translator = str.maketrans('', '', string.punctuation)
        q_list = []
        for questions in np_question_list:
            x = questions.translate(translator)
            x = x[0].upper() + x[1:].lower() + "?"
            q_list.append(x)

        return q_list


if __name__ == "__main__":

    file = path.expanduser('E:/Level 6/FYP/Implementation/DataSet/Mount Everest/test.txt')
    print(Transducer.np_based_questions(file))
