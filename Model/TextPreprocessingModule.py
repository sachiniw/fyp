from os import path
import re
from nltk.tokenize import sent_tokenize
from nltk.tag import StanfordNERTagger
from nltk.parse.stanford import StanfordParser
from nltk.tree import ParentedTree, Tree

file = path.expanduser('E:/Level 6/FYP/Implementation/DataSet/Mount Everest/test.txt')
st = StanfordNERTagger('E:/stanford-ner-2018-02-27/classifiers/english.all.3class.distsim.crf.ser.gz',
                       'E:/stanford-ner-2018-02-27/stanford-ner.jar', encoding='utf-8')
#  Create parser object
scp = StanfordParser(path_to_jar='E:/stanford-parser-full-2018-02-27/stanford-parser.jar',
                     path_to_models_jar='E:/stanford-parser-full-2018-02-27/stanford-parser-3.9.1-models.jar')


class TextPreprocessor:

    # splits a text input to paragraphs
    def para_tokenize(input_file):
        para_list = []
        # read the file and remove any errors
        with open(input_file, 'rb') as fp:
            data = fp.read().decode('utf8')

        # split the whole text to separate paragraphs by newlines
        paragraphs = data.split('\r\n\r\n')
        for para in paragraphs:
            para_list.append(para)

        return para_list

    # splits the paragraphs of the text input to separate sentences
    def text_tokenize(paragraph):
        sent_list = []
        # sentence tokenization
        sent = sent_tokenize(paragraph)
        if len(sent) != 1:
            for s in sent:
                x = re.sub(r'\([^)]*\)', '', s)
                sent_list.append(x)

        return sent_list

    # separate the topics of the text
    def get_topics(file):
        topic_list = []
        paragraphs = TextPreprocessor.para_tokenize(file)
        # sentence tokenization
        for para in paragraphs:
            sent = sent_tokenize(para)
            if len(sent) == 1:
                topic_list.append(sent)

        return topic_list

    # NER tagging
    def ner_tagger(sent):
        classified_text = st.tag(sent.split())

        return classified_text

    #  Get parse tree
    def sent_parse(sent):
        result = list(scp.raw_parse(sent))
        leaf = []
        for line in result:
            for l in line:
                leaf.append(l)

        s = "{0}".format(", ".join(str(i) for i in leaf))
        parse_tree = Tree.fromstring(s)
        parsed_sent = ParentedTree.convert(parse_tree)

        return parsed_sent
