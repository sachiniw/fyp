import nltk
import string
from os import path

from sklearn.feature_extraction.text import TfidfVectorizer
from nltk.stem.porter import PorterStemmer

from Model.SentenceSimplificationModule import SentenceSimplifier


class Feature04:

    def tokenize(text):
        tokens = nltk.word_tokenize(text)
        stems = []
        for item in tokens:
            stems.append(PorterStemmer().stem(item))
        return stems

    def get_frequency_weightage(paragraphs):
        sent_list = []
        frequency_weight = 0
        frequency_weight_list = []

        translator = str.maketrans('', '', string.punctuation)
        for para in paragraphs:
            for s in para:
                x = s.translate(translator)
                sent_list.append(x)

        tfidf = TfidfVectorizer(tokenizer=Feature04.tokenize, stop_words='english')
        tfs = tfidf.fit_transform(sent_list)

        feature_names = tfidf.get_feature_names()

        for num in range(0, len(sent_list)):
            feature_index = tfs[num, :].nonzero()[1]
            tf_idf_scores = zip(feature_index, [tfs[num, n] for n in feature_index])
            count = 0
            for w, s in [(feature_names[i], s) for (i, s) in tf_idf_scores]:
                count += 1

            frequency_weight = count / len(nltk.word_tokenize(sent_list[num]))
            frequency_weight_list.append((sent_list[num], round(frequency_weight, 4)))

        return frequency_weight_list


if __name__ == "__main__":
    file = path.expanduser('E:/Level 6/FYP/Implementation/DataSet/Mount Everest/test.txt')
    simplified_sent_list = SentenceSimplifier.simplifier(file)
    print(Feature04.get_frequency_weightage(simplified_sent_list))


