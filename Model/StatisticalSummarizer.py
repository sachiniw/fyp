from os import path

from Model.F1_SentencePositionCalculation import Feature01
from Model.F2_TopicFeatureCalculation import Feature02
from Model.F3_NumericFeatureCalculation import Feature03
from Model.F4_FrequencyWeightageCalculation import Feature04
from Model.F5_ProperNounCalculation import Feature05
from Model.F6_SentenceLengthCalculation import Feature06

from Model.SentenceSimplificationModule import SentenceSimplifier
from Model.TextPreprocessingModule import TextPreprocessor


class StatisticalSummarizer:


    def find_score(f1_list, f2_list, f3_list, f4_list, f5_list, f6_list):
        min_max_list = []
        max_f1 = max(f1_list, key=lambda item: item[1])[1]
        min_f1 = min(f1_list, key=lambda item: item[1])[1]
        min_max_list.append((min_f1,max_f1))
        max_f2 = max(f2_list, key=lambda item: item[1])[1]
        min_f2 = min(f2_list, key=lambda item: item[1])[1]
        min_max_list.append((min_f2, max_f2))
        max_f3 = max(f3_list, key=lambda item: item[1])[1]
        min_f3 = min(f3_list, key=lambda item: item[1])[1]
        min_max_list.append((min_f3, max_f3))
        max_f4 = max(f4_list, key=lambda item: item[1])[1]
        min_f4 = min(f4_list, key=lambda item: item[1])[1]
        min_max_list.append((min_f4, max_f4))
        max_f5 = max(f5_list, key=lambda item: item[1])[1]
        min_f5 = min(f5_list, key=lambda item: item[1])[1]
        min_max_list.append((min_f5, max_f5))
        max_f6 = max(f6_list, key=lambda item: item[1])[1]
        min_f6 = min(f6_list, key=lambda item: item[1])[1]
        min_max_list.append((min_f6, max_f6))

        low_value_list = []

        for v in min_max_list:
            l = (v[0]+v[1])/2
            low_value_list.append(l)

        f1_vectors = []
        for i in f1_list:
            if i[1] <= low_value_list[0]:
                f1_vectors.append(0)
            else:
                f1_vectors.append(1)

        f2_vectors = []
        for i in f2_list:
            if i[1] <= low_value_list[1]:
                f2_vectors.append(0)
            else:
                f2_vectors.append(1)

        f3_vectors = []
        for i in f3_list:
            if i[1] <= low_value_list[2]:
                f3_vectors.append(0)
            else:
                f3_vectors.append(1)

        f4_vectors = []
        for i in f4_list:
            if i[1] <= low_value_list[3]:
                f4_vectors.append(0)
            else:
                f4_vectors.append(1)

        f5_vectors = []
        for i in f5_list:
            if i[1] <= low_value_list[4]:
                f5_vectors.append(0)
            else:
                f5_vectors.append(1)

        f6_vectors = []
        for i in f6_list:
            if i[1] <= low_value_list[5]:
                f6_vectors.append(0)
            else:
                f6_vectors.append(1)

        sentence_vectors = []
        for j in range(0, len(f1_list)):
            sentence_vectors.append((f1_list[j][0], f1_vectors[j], f2_vectors[j], f3_vectors[j], f4_vectors[j],
                                     f5_vectors[j], f6_vectors[j]))

        rated_sent_list = []
        for value in sentence_vectors:
            total = value[1] + value[2] + value[3] + value[4] + value[5] + value[6]
            rated_sent_list.append((value[0], total))

        return rated_sent_list

    def summarizer(file_name):
        file_ = path.expanduser(file_name)
        simplified_sent_list = SentenceSimplifier.simplifier(file_)
        topic_list = TextPreprocessor.get_topics(file_)
        list_1 = Feature01.get_sent_position_rate(simplified_sent_list)
        list_2 = Feature02.get_topic_feature_rate(simplified_sent_list, topic_list)
        list_3 = Feature03.get_numeric_content_rate(simplified_sent_list)
        list_4 = Feature04.get_frequency_weightage(simplified_sent_list)
        list_5 = Feature05.get_proper_noun_rate(simplified_sent_list)
        list_6 = Feature06.get_sent_length_rate(simplified_sent_list)

        rated_list = StatisticalSummarizer.find_score(list_1, list_2, list_3, list_4, list_5, list_6)

        summary = []
        for sent in rated_list:
            if sent[1] > 1 :
                summary.append(sent[0])
        # print(summary)
        return summary


if __name__ == "__main__":
    # file = path.expanduser('E:/Level 6/FYP/Implementation/DataSet/Taj Mahal/Layout And Architecture.txt')
    StatisticalSummarizer.summarizer('E:/Level 6/FYP/Implementation/DataSet/Mount Everest/test.txt')