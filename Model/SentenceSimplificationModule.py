from os import path
import nltk
from nltk.util import breadth_first
import string

from Model.TextPreprocessingModule import TextPreprocessor


class SentenceSimplifier:

    # Move leading Prepositional Phrases of a sentence to the end of the end of main Verb Phrase of parse tree
    def move_leading_PP(parsedSent):
        noduleList = []
        for node in breadth_first(parsedSent, -1):
            if node.label() == 'S':  # checks if root node is S
                for nodule in node:
                    noduleList.append(nodule.label())
                    if noduleList[0] != 'PP':
                        break

                    if nodule.label() == 'PP':
                        pp = nodule.leaves()
                        del parsedSent[nodule.treeposition()]

                    if nodule.label() == 'VP':
                        parsedSent.append(nltk.tree.ParentedTree('PP', pp))

                    if nodule.label() == '.':
                        del parsedSent[nodule.treeposition()]

        return parsedSent

    # function to remove adjunct modifiers and discourse markers
    def del_adjuncts(parsedSent):
        for node in breadth_first(parsedSent, -1):
            if node.label() == 'S':  # checks if root node is S
                for nodule in node:
                    if nodule.label() == 'ADVP':
                        del parsedSent[nodule.treeposition()]

        return parsedSent

    # delete subtrees with S/SBAR and make new parse trees from them
    def del_subtree(parsedSent):
        for node in breadth_first(parsedSent, -1):
            # Separated sub tree containing list
            separated_trees = []
            if node.label() == 'S':  # checks if root node is S
                for nodule in node:
                    if nodule.label() in ['SBAR', 'S']:
                        separated_trees.append(nodule.leaves())
                        del parsedSent[nodule.treeposition()]

        if len(separated_trees) != 0:
            new_parse_trees = []
            for st in separated_trees:
                new_tree = TextPreprocessor.sent_parse(' '.join(st))
                new_parse_trees.append(new_tree)
            new_parse_trees.append(parsedSent)
            parse_tree = new_parse_trees
        else:
            parse_tree = [parsedSent]

        return parse_tree

    # check the presence of noun and verb phrases in proper order
    def check_sentence_structure(parsedSent):
        noduleList = []
        for node in breadth_first(parsedSent, -1):
            if node.label() == 'S':  # checks if root node is S
                for nodule in node:
                    noduleList.append(nodule.label())

        if any(['NP', 'VP'] == noduleList[i:i + 2] for i in range(0, len(noduleList))):
            valid_sentence = True
        else:
            valid_sentence = False

        return valid_sentence

    # function to simplify text input
    def simplifier(file_name):
        translator = str.maketrans('', '', string.punctuation)
        paragraphs = TextPreprocessor.para_tokenize(file_name)
        sentences_per_para = []
        for para in paragraphs:
            simplified_sent_list = []
            sentences = TextPreprocessor.text_tokenize(para)
            for sent in sentences:
                parsed_sent = TextPreprocessor.sent_parse(sent)
                for node in breadth_first(parsed_sent, -1):
                    if node.label() == 'S':  # checks if root node is S
                        SentenceSimplifier.move_leading_PP(parsed_sent)
                        SentenceSimplifier.del_adjuncts(parsed_sent)
                        subtree_check = SentenceSimplifier.del_subtree(parsed_sent)
                        for parsed_tree in subtree_check:
                            if SentenceSimplifier.check_sentence_structure(parsed_tree):
                                s = ' '.join(parsed_tree.leaves())
                                x = s.translate(translator) + '.'
                                simplified_sent_list.append(x)
            sentences_per_para.append(simplified_sent_list)

        return sentences_per_para


if __name__ == "__main__":
    f_name = path.expanduser('E:/Level 6/FYP/Implementation/DataSet/Mount Everest/test.txt')
    x = SentenceSimplifier.simplifier(f_name)
    print(*x, sep='\n')
