from os import path
import nltk
import string
import re

from Model.SentenceSimplificationModule import SentenceSimplifier


class Feature03:

    def get_numeric_content_rate(paragraphs):
        # tuple list to store the numeric rating of each sentence
        numeric_rate_list = []
        numeric_feature_rate = 0
        translator = str.maketrans('', '', string.punctuation)

        # tokenization of sentences of each paragraph
        for para in paragraphs:
            for s in para:
                x = s.translate(translator)
                numerical_matches = re.findall('\d+', x)
                numeric_feature_rate = len(numerical_matches) / len(nltk.word_tokenize(x))
                numeric_rate_list.append((s, round(numeric_feature_rate, 4)))

        return numeric_rate_list


if __name__ == "__main__":
    file = path.expanduser('E:/Level 6/FYP/Implementation/DataSet/Mount Everest/test.txt')
    simplified_sent_list = SentenceSimplifier.simplifier(file)
    print(Feature03.get_numeric_content_rate(simplified_sent_list))