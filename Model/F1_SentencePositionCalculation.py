from os import path
from Model.SentenceSimplificationModule import SentenceSimplifier


class Feature01:

    def get_sent_position_rate(paragraphs):

        sent_postion_rate_list = []
        sp_rate = 0
        for pg in paragraphs:
            for i in range(0, len(pg)):
                sp_rate = (len(pg) - i) / len(pg)
                sent_postion_rate_list.append((pg[i], round(sp_rate, 4)))

        return sent_postion_rate_list


if __name__ == "__main__":
    file = path.expanduser('E:/Level 6/FYP/Implementation/DataSet/Mount Everest/test.txt')
    simplified_sent_list = SentenceSimplifier.simplifier(file)
    print(Feature01.get_sent_position_rate(simplified_sent_list))