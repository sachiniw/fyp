from os import path
import nltk
import string
from nltk.tag import StanfordNERTagger

from Model.TextPreprocessingModule import TextPreprocessor
from Model.SentenceSimplificationModule import SentenceSimplifier


class Feature05:

    def get_proper_noun_rate(paragraphs):
        # tuple list to store the proper noun rating of each sentence
        pn_rate_list = []
        pn_feature_rate = 0
        translator = str.maketrans('', '', string.punctuation)

        for para in paragraphs:
            for s in para:
                x = s.translate(translator)
                tagged_sent = TextPreprocessor.ner_tagger(x)
                count = 0

                for tw in tagged_sent:
                    if tw[1] != 'O':
                        count += 1

                pn_feature_rate = round((count / len(nltk.word_tokenize(x))), 4)
                pn_rate_list.append((s, pn_feature_rate))

        return pn_rate_list


if __name__ == "__main__":
    file = path.expanduser('E:/Level 6/FYP/Implementation/DataSet/Mount Everest/test.txt')
    simplified_sent_list = SentenceSimplifier.simplifier(file)
    print(Feature05.get_proper_noun_rate(simplified_sent_list))

