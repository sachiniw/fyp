from os import path
import nltk
import string

from Model.SentenceSimplificationModule import SentenceSimplifier


class Feature06:

    def get_sent_length_rate(paragraphs):
        # list to store sentences
        sent_list = []
        # list to store length of each sentence
        sent_length = []
        # list to store the tuple of sentence and sentence length rate
        sent_length_rate_list = []
        length_feature_rate = 0

        translator = str.maketrans('', '', string.punctuation)
        for para in paragraphs:
            for s in para:
                x = s.translate(translator)
                sent_list.append(x)

        for sentence in sent_list:
            sent_length.append((sentence, len(nltk.word_tokenize(sentence))))

        max_length = max(sent_length, key=lambda item: item[1])[1]

        # calculating sentence length score
        for sl in sent_length:
            length_feature_rate = round((sl[1] / max_length), 4)
            sent_length_rate_list.append((sl[0], length_feature_rate))

        return sent_length_rate_list

if __name__ == "__main__":
    file = path.expanduser('E:/Level 6/FYP/Implementation/DataSet/Mount Everest/test.txt')
    simplified_sent_list = SentenceSimplifier.simplifier(file)
    print(Feature06.get_sent_length_rate(simplified_sent_list))


